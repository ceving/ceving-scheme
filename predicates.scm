(define-syntax predicates
  (syntax-rules ()
    ((_ =? lst)
     (map (lambda (x) (lambda (y) (=? x y))) lst))))
