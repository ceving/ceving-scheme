(define (matching-all-unordered? needed given)
  (let ((needed (cons '() needed)))
    (let next-given ((given given))
      (if (null? given)
	  (null? (cdr needed))
	  (let next-needed ((needed needed))
	    (if (null? (cdr needed))
		#f
		(if ((cadr needed) (car given))
		    (begin
		      (set-cdr! needed (cddr needed))
		      (next-given (cdr given)))
		    (next-needed (cdr needed)))))))))
