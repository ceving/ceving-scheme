(define-syntax test
  (syntax-rules ()
    ((_ (predicate expression expectation) ...)
     (let ((tests 0)
	   (errors 0))
       (let ((result expression))
	 (set! tests (+ tests 1))
	 (display "Test ")
	 (display tests)
	 (display ": ")
	 (if (predicate result expectation)
	     (display "OK")
	     (begin
	       (display "ERROR")
	       (display ": ")
	       (display 'expression)
	       (display " -> ")
	       (display result)
	       (display " expecting ")
	       (display 'expectation)
	       (set! errors (+ errors 1))))
	 (display ".")
	 (newline))
       ...
       (display "Summary: ")
       (display tests)
       (display " tests, ")
       (display errors)
       (display " errors.")
       (newline)))))
