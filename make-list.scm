(define-syntax make-list*
  (syntax-rules ()
    ((_ n expr)
     (let loop ((i n))
       (if (> i 0)
	   (cons expr (loop (- i 1)))
	   '())))))

;; Example:
;; 
;; (define (make-inc i)
;;   (lambda ()
;;     (let ((x i))
;;       (set! i (+ i 1))
;;       x)))
;; 
;; (let ((inc (make-inc 0)))
;;   (make-list 3 (inc)))     -> (0 0 0)
;; 
;; (let ((inc (make-inc 0)))
;;   (make-list* 3 (inc)))    -> (0 1 2)
